﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Collections;
using Unity.Physics;

public class TimeDestroySystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = Time.DeltaTime;

        Entities
            .WithoutBurst()
            .WithStructuralChanges()
            //.WithName("TimeDestroySystem")
            .ForEach((Entity entity,
                        ref Translation position,
                        ref LifeTimeData lifeTimeData) =>
            {
                lifeTimeData.lifeTime -= deltaTime;

                if (lifeTimeData.lifeTime <= 0.0f)
                {
                    EntityManager.DestroyEntity(entity);
                }

            }).Run();

        Entities
            .WithoutBurst()
            .WithStructuralChanges()
            //.WithName("TimeDestroySystem")
            .ForEach((Entity entity,
                        ref Translation position,
                        ref VirusData virusData) =>
            {
                if (!virusData.alive)
                {
                    for (int i = 0; i < 100; i++)
                    {
                        float3 offset = (float3)UnityEngine.Random.insideUnitSphere * 2.0f;
                        float3 randomDirection = new float3(UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1));

                        var splat = ECSManager.manager.Instantiate(ECSManager.witheBlood);

                        ECSManager.manager.SetComponentData(splat, new Translation { Value = position.Value + offset });
                        ECSManager.manager.SetComponentData(splat, new PhysicsVelocity { Linear = randomDirection * 2 });
                    }

                    EntityManager.DestroyEntity(entity);
                }


            }).Run();

        return inputDeps;
    }
}
